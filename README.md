# Stockpile

Stockpile is a simple Go-based app for vendoring Go Modules mentioned in your go.sum file. To install:

`go get gitlab.com/pokstad1/stockpile`

For command documentation, run: `go doc gitlab.com/pokstad1/stockpile`

# Use Cases

Run stockpile to make sure all project dependencies are captured, then use the system of your choice to capture the dependencies as an artifact (see the "hoard" command).

Additionally, stockpile can also generate static directory listings for your GOPROXY web site, so that you can navigate through it (see the "index" subcommand).

## GitLab Vendoring

Vendoring dependencies on GitLab can be done using GitLab pages and GitLab CI cache. This enables efficient fetching of dependencies and allows dependencies to be fetched via the GOPROXY API.

A [sample `.gitlab-ci.yml`](.gitlab-ci.yml) can be found in this project. Pay extra attention to the pages job:

```yaml
pages:
  script:
    - go install gitlab.com/pokstad1/stockpile
    # Disable proxy while stockpiling
    - GOPROXY="" ${GOPATH}/bin/stockpile hoard
    - cp -r .go/pkg/mod/cache/download public
    - ${GOPATH}/bin/stockpile index -root=public
  artifacts:
    paths:
    - public
  only:
  - master
```

Once the vendored dependencies are deployed as a GitLab Pages artifact, you can configure your Go tooling to use it as a GOPROXY. For example, to build stockpile while using its own GOPROXY to resolve dependencies:

```
go get gitlab.com/pokstad1/stockpile
export GO111MODULE=on
GOPROXY=https://pokstad1.gitlab.io/stockpile go mod download
go install
```
