// Command stockpile will parse a go.sum file and run `go mod download` for each
// module encountered. Any errors encountered will result in aborting the
// operation and returning a non-zero status.
//
// Usage:
//   stockpile hoard [-sum PATH_TO_GO_SUM]
//   stockpile index -root=PATH_TO_ARTIFACT
//
// Note: you may need to turn on modules functionality for stockpile to work
// correctly:
//   GO111MODULE=on stockpile
//
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"text/template"
)

var (
	hoardFlagSet = flag.NewFlagSet("hoard", flag.ExitOnError)
	sumPath      = hoardFlagSet.String("sum", "go.sum", "path to go.sum file you wish to archive")

	indexFlagSet = flag.NewFlagSet("index", flag.ExitOnError)
	rootPath     = indexFlagSet.String("root", "", "root path to recursively index static assets")

	sumModRegex = regexp.MustCompile(`^(.+?) (.+?)/go.mod (.+?)$`)
)

const sumModRegexTokens = 3

func main() {
	validCmds := []string{"hoard", "index"}

	if len(os.Args) < 2 {
		log.Fatalf("must provide subcommand: %+v", validCmds)
	}

	switch os.Args[1] {
	case validCmds[0]:
		hoard()
	case validCmds[1]:
		index()
	default:
		log.Fatalf("must provide subcommand: %+v", validCmds)
	}

}

func hoard() {
	hoardFlagSet.Parse(os.Args[2:])
	if *sumPath == "" {
		log.Fatalf("invalid path: %q", *sumPath)
	}

	sumF, err := os.Open(*sumPath)
	if err != nil {
		log.Fatal(err)
	}
	defer sumF.Close()

	scanner := bufio.NewScanner(sumF)

	for scanner.Scan() {
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		line := scanner.Text()

		matches := sumModRegex.FindStringSubmatch(line)
		if len(matches) != 4 {
			log.Printf("skipping go.sum line %q %#v", line, matches)
			continue
		}

		module, version := matches[1], matches[2]

		if err := goModDownload(module, version); err != nil {
			log.Fatalf("unable to download %s@%s: %q", module, version, err)
		}
	}
}

func goModDownload(module, version string) error {
	cmd := exec.Command("go", "mod", "download", fmt.Sprintf("%s@%s", module, version))
	cmd.Stdout = os.Stderr
	cmd.Stderr = os.Stderr

	log.Printf("Running command %+v", cmd.Args)

	return cmd.Run()
}

func index() {
	indexFlagSet.Parse(os.Args[2:])
	if *rootPath == "" {
		log.Fatalf("invalid index path: %q", *rootPath)
	}

	if err := generateHTMLs(*rootPath); err != nil {
		log.Fatal(err)
	}
}

var indexHTMLTmpl = template.Must(template.New("name").Parse(`<!DOCTYPE html>
<html>
<head></head>
<body>
    <ul>
{{range .}}<li><a href="{{ .Link }}">{{ .Display }}</a></li>{{end}}
    </ul>
</body>
</html>
`))

func generateHTMLs(root string) error {
	return filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() {
			return nil
		}

		indexPath := filepath.Join(path, "index.html")
		log.Printf("creating index file %s", indexPath)
		dst, err := os.OpenFile(indexPath, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0755)
		if err != nil {
			return err
		}
		defer dst.Close()

		infos, err := ioutil.ReadDir(path)
		if err != nil {
			return err
		}

		links := make([]struct {
			Link    string
			Display string
		}, len(infos))

		for i, info := range infos {
			links[i].Link = info.Name()
			links[i].Display = info.Name()

			if info.IsDir() {
				links[i].Link = filepath.Join(info.Name(), "index.html")
			}
		}

		return indexHTMLTmpl.Execute(dst, links)
	})
}
